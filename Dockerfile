FROM ubuntu:bionic

ARG DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    software-properties-common \
    build-essential \
    apt-transport-https \
    ca-certificates \
    gnupg \
    ccache \
    wget \
    ninja-build \
    git \
    curl \
    tar \
    htop \
    pigz \
    liblapack-dev \
    libblas-dev \
    zlib1g-dev

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null \
    | apt-key add - \
    && apt-add-repository -y 'deb https://apt.kitware.com/ubuntu/ bionic main' \
    && apt-get update \
    && apt-get -y install cmake

WORKDIR /tmp/

############ Installing and setting up ANTs (Currently using cookpa's branch, change to master after merge)
RUN echo "Compiling ANTs Latest" && \
    git clone  --single-branch --branch sharpeningOptions https://github.com/ANTsX/ANTs.git && \
    cd ANTs  && \
    mkdir /tmp/ANTs/Build  && \
    mkdir /opt/ANTs  && \
    cd /tmp/ANTs/Build  && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/ANTs ../  && \
    make -j 12  && \
    cd ANTS-build  && \
    make install  && \
    rm -rf /tmp/ANTs

ENV ANTSPATH=/opt/ANTs/bin
ENV PATH=$ANTSPATH:$PATH
ENV LD_LIBRARY_PATH=/opt/ANTs/lib:$LD_LIBRARY_PATH

############ Installing and setting up FaceOff
WORKDIR /opt/

RUN echo "Installing Defacing Script" && \
    git clone https://gitlab.com/skash/faceoff.git && \
    chmod +x /opt/faceoff/FaceOff

ENV PATH=/opt/faceoff:$PATH

############ Installing and setting up c3d
RUN echo "Installing c3d v 1.1.0" && \
    mkdir -p /opt/c3d && \
    curl -sSL "https://sourceforge.net/projects/c3d/files/c3d/Experimental/c3d-1.1.0-Linux-gcc64.tar.gz" \
    | tar -xzC /opt/c3d --strip-components 1

ENV PATH=/opt/c3d/bin:$PATH

WORKDIR /data

CMD ["/bin/bash"]