## Dockerised ANTs

`git clone https://gitlab.com/skash/antsDocker.git`

`cd antsDocker`

`docker build -t ants:latest .`

Contents: ANTs utilities, FaceOff defacing utility and C3D multi-purpose utilities on Ubuntu 18.04.4 LTS (Bionic Beaver).

#### Example use cases

##### ANTs
`docker run --rm -it -v $PWD:/data ants antsRegistrationSyNQuick.sh -d 3 -f /data/fixed.nii.gz -m /data/moving.nii.gz -o /data/moving_to_fixed_ -t s -p f -j 1 -n 12`

##### FaceOff
`docker run --rm -it -v $PWD:/data ants FaceOff -i /data/T1w_to_be_defaced.nii.gz -n 12`

##### C3D
`docker run --rm -it -v $PWD:/data ants c3d /data/data_orig.nii.gz -resample 200% -interpolation Sinc -type uint -o /data/data_upsampled.nii.gz`
